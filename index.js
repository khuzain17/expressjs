const express = require ('express');

const app = express();
const port = 3000;

app.get('/', (req, res) => {
    res.send('Hello my This is root endpoint');
});

app.get('/home', (req, res) => {
    res.send('Here is home endpoint');
    res.send('Put home page web here');
});

app.get('/about', (req, res)=> {
    res.send('you can fill company profile here!');
});

app.post('/get', (req,res)=>{
    res.send('add comment success!');
});

app.get('/next', (req,res) => {
    res.status(404);
    res.send({ 
        data: "home",
        value: ['satu', 'dua', 'tiga'],
    });
});

app.get('/login', (req,res)=> {
    res.send('bagian login');
});

app.post('/login', (req,res)=> {
    res.send('you can add json for login here');
});

app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
